'use strict';

var index       = require('./utils'),
    getLogger   = index.getLogger,
    async       = require('async'),
    mysql       = require('mysql');

exports.mysql = mysql;

var connection,
    connectionTimer1,
    connectionTimer2;

const MYSQL_WAIT_TIMEOUT = 300000;

// MySQLDB functions
var executeQuery = function(request, queryStr, queryData, callback) {
    getLogger().debug('EXECUTE QUERY :');
    getLogger().debug(request.format(queryStr, queryData));
    
    request.query(queryStr, queryData, function(err, recordset) {
        if (err) getLogger()._error(err);
        getLogger().trace(recordset);
        callback(err, recordset);
    });
};
exports.executeQuery = executeQuery;

var mysqlDBConnect = function(connectionOptions, callback) {
    var timeout = MYSQL_WAIT_TIMEOUT;
    getLogger().info('Establishing DB Connection');
    connection = mysql.createConnection(connectionOptions); 

    if ('reconnect_timeout' in connectionOptions) {
        timeout = connectionOptions.reconnect_timeout;
    };

    connection.connect(function(err) {              
        if (err) {                                  
            getLogger()._error('DB Connection Error !');
            getLogger()._error(err);
            connectionTimer1 = setTimeout(function() {
                mysqlDBConnect(connectionOptions, callback);
            }, timeout); 
        } else {
            getLogger().info('DB Connection successfull');
            if (callback) {
                callback();
            };
        };                                  
    });                                     
                                          
    connection.on('error', function(err) {
        // console.log('db error', err);
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {              // Connection to the MySQL server is usually
            getLogger().info('DB Connection lost !');               // lost due to either server restart, or a
            connectionTimer2 = setTimeout(function() {              // connnection idle timeout (the wait_timeout
                mysqlDBConnect(connectionOptions, callback);        // server variable configures this)
            }, timeout);
        } else {
            getLogger()._error(err);                                
            throw err;                                  
        }
    });
};
exports.mysqlDBConnect = mysqlDBConnect;

var getConnection = function() {
    return connection;
};
exports.getConnection = getConnection;

var closeConnection = function(callback) {
    getLogger().info('DB closing connection...');
    clearTimeout(connectionTimer1);
    clearTimeout(connectionTimer2);

    connection.end(function(err) {
        if (err) {
            connection.destroy();
        }

        if(callback) {callback(null)};
    })
};
exports.closeConnection = closeConnection;