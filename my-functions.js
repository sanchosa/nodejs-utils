'use strict';

// different functions

// генератор случайной строки
var strRandom = function(strLength) {
    var result       = '';
    var words        = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
    var max_position = words.length - 1;
    var position;
        
    for(var i = 0; i < strLength; ++i ) {
        position = Math.floor ( Math.random() * max_position );
        result = result + words.substring(position, position + 1);
    }

    return result;
}
exports.strRandom = strRandom;

// Функция проверки строки на valid JSON 
var validateJSON = (data => {
    var obj;
    try {
        obj = JSON.parse(data)
    }
    catch(err) {
        return data
    }
    return obj
})
exports.validateJSON = validateJSON;