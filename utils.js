'use strict';

var path   = require('path'),
    log4js = require('log4js'),
    logger;

var configureLogger = function(options) {
    console.log('CONFIGURE LOGGER :');
    console.log(options);
    if (options) {
        log4js.configure(options, {});
    } else {
        log4js.configure(path.join(__dirname, 'log4js.json'), {});
    }
    logger = log4js.getLogger();
    logger._error = logger.error;
    exports.logger = logger;
};
configureLogger();
exports.configureLogger = configureLogger;

var getLogger = function() {
    return logger;
};
exports.getLogger = getLogger;

var mysqlF   = require('./mysql'),
    httpF    = require('./http'),
    expressF = require('./express'),
    rmqF     = require('./rmq'),
    myF      = require('./my-functions');
    // nodemailerF = require('./nodemailer');

// MySQLDB functions
exports.mysql = mysqlF.mysql;
exports.executeQuery = mysqlF.executeQuery;
exports.mysqlDBConnect = mysqlF.mysqlDBConnect;
exports.getConnection = mysqlF.getConnection;
exports.closeConnection = mysqlF.closeConnection;

// http Functions
exports.setHttpOptions = httpF.setHttpOptions;
exports.httpGet = httpF.httpGet;
exports.httpPost = httpF.httpPost;
exports.httpDelete = httpF.httpDelete;
exports.httpPut = httpF.httpPut;

// https Functions
exports.setHttpsOptions = httpF.setHttpsOptions;
exports.httpsGet = httpF.httpsGet;
exports.httpsPost = httpF.httpsPost;
exports.httpsDelete = httpF.httpsDelete;
exports.httpsPut = httpF.httpsPut;

// Express functions
exports.sendResponse = expressF.sendResponse;

// RabbitMQ functions
exports.createRMQChannel = rmqF.createRMQChannel;
exports.assertRMQ = rmqF.assertRMQ;
exports.sendRMQMessage = rmqF.sendRMQMessage;
exports.consumeRMQMessage = rmqF.consumeRMQMessage;
exports.disconnectRMQ = rmqF.disconnectRMQ;
exports.sendRMQack = rmqF.sendRMQack;
exports.sendRMQnack = rmqF.sendRMQnack;
exports.setRMQData = rmqF.setRMQData;
exports.prepareQueue = rmqF.prepareQueue;
exports.getMQConnection = rmqF.getMQConnection;
exports.getMQChannel = rmqF.getMQChannel;
exports.createCheckQueueInterval = rmqF.createCheckQueueInterval;
exports.stopCheckQueueInterval = rmqF.stopCheckQueueInterval;

// different functions
exports.strRandom = myF.strRandom;
exports.validateJSON = myF.validateJSON;