'use strict';

var index       = require('./utils'),
    getLogger   = index.getLogger;

// Express functions
var sendResponse = function(err, data, res) {
    var status,
        message;
    
    if (err) {
        getLogger()._error(err);
        
        if ('errno' in err) {
            switch (err.errno) {
                case 1062 :
                    status = 203;
                    message = err;
                    break;
                default :
                    status = 500;
                    message = err;        
                    break;
            };
        } else {
            status = 500;
            message = err;
        };
    } else {
        message = data;
        if (data) {
            status = 200;
        } else {
            status = 204;
        }
    };
    
    getLogger().debug("sendResponse : status - " + status + ", message - " + message + ";");
    res.status(status).send(message);
};
exports.sendResponse = sendResponse;