'use strict';

var index       = require('./utils'),
    getLogger   = index.getLogger,
    http        = require('http'),
    https       = require('https');

var httpOptions = {
    hostname : '127.0.0.1',
    port     : 80,
    path     : '/',
    method   : '',
};

var httpsOptions = {
    hostname : '127.0.0.1',
    port     : 443,
    path     : '/',
    method   : '',
};

// http Functions
var setHttpOptions = function(options) {
    getLogger().debug('SET HTTP OPTIONS:');
    setOptions(httpOptions, options);
};
exports.setHttpOptions = setHttpOptions;

var httpGet = function(url, callback) {
    sendRequest(http, 'GET', url, null, callback);
};
exports.httpGet = httpGet;

var httpDelete = function(url, callback) {
    sendRequest(http, 'DELETE', url, null, callback);
};
exports.httpDelete = httpDelete;

var httpPost = function(url, data, callback) {
    sendRequest(http, 'POST', url, data, callback);
};
exports.httpPost = httpPost;

var httpPut = function(url, data, callback) {
    sendRequest(http, 'PUT', url, data, callback);
};
exports.httpPut = httpPut;

// https Functions
var setHttpsOptions = function(options) {
    getLogger().debug('SET HTTPS OPTIONS:');
    setOptions(httpsOptions, options);
};
exports.setHttpsOptions = setHttpsOptions;

var httpsGet = function(url, callback) {
    sendRequest(https, 'GET', url, null, callback);
};
exports.httpsGet = httpsGet;

var httpsDelete = function(url, callback) {
    sendRequest(https, 'DELETE', url, null, callback);
};
exports.httpsDelete = httpsDelete;

var httpsPost = function(url, data, callback) {
    sendRequest(https, 'POST', url, data, callback);
};
exports.httpsPost = httpsPost;

var httpsPut = function(url, data, callback) {
    sendRequest(https, 'PUT', url, data, callback);
};
exports.httpsPut = httpsPut;

// выставление глобальных опций
var setOptions = function(obj, options) {
    getLogger().debug(options);
    if (options) {
        if ('hostname' in options) {
            obj.hostname = options.hostname;
        };
        if ('port' in options) {
            obj.port = options.port;
        };
        if ('path' in options) {
            obj.path = options.path;
        };
        if ('headers' in options) {
            obj.headers = options.headers;
        };
    };
};

var addHeaders = function(obj, headers) {
    if ('headers' in obj) {
        Object.keys(headers).forEach(function(key) {
            obj.headers[key] = headers[key];
        });
    } else {
        obj.headers = headers;
    }
};

// проверить работоспособность функции sendRequest
var sendRequest = function(module, method, url, data, callback) {
    var result = '';
    var postData = JSON.stringify(data);
    var options = (module == http) 
        ? JSON.parse(JSON.stringify(httpOptions)) 
        : JSON.parse(JSON.stringify(httpsOptions));

    options.path += url;
    
    switch(method) {
        case 'GET' :
            options.method = 'GET';
            addHeaders(options, {'Content-Length': Buffer.byteLength(postData)});
            // options.headers = {
            //     'Content-Length': Buffer.byteLength(postData)
            // };
            break;
        case 'DELETE' :
            options.method = 'DELETE';
            addHeaders(options, {'Content-Length': Buffer.byteLength(postData)});
            // options.headers = {
            //     'Content-Length': Buffer.byteLength(postData)
            // };
            break;
        case 'POST' :
            options.method = 'POST';
            addHeaders(options, {'Content-Type': 'application/json', 'Content-Length': Buffer.byteLength(postData)});
            // options.headers = {
            //     'Content-Type': 'application/json',
            //     'Content-Length': Buffer.byteLength(postData)
            // };
            break;
        case 'PUT' :
            options.method = 'PUT';
            addHeaders(options, {'Content-Type': 'application/json', 'Content-Length': Buffer.byteLength(postData)});
            // options.headers = {
            //     'Content-Type': 'application/json',
            //     'Content-Length': Buffer.byteLength(postData)
            // };
            break;
    };

    getLogger().info('SEND REQUEST : ');
    getLogger().trace('DATA : ');
    getLogger().trace(data);
    getLogger().trace('OPTIONS : ');
    getLogger().trace(options);

    var req = module.request(options, function(res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            getLogger().trace('BODY: ' + chunk);
            result += chunk;
        });
        res.on('end', function() {
            getLogger().info('No more data in response.');
            getLogger().trace('RETURNED RESULT : ');
            result = {
                response : index.validateJSON(result),
                statusCode: res.statusCode
            };
            getLogger().trace(result);
            callback(null, result);
        })
    });

    req.on('error', function(e) {
        getLogger().error('problem with request: ' + e.message);
        callback(e, result);
    });

    // write data to request body
    req.write(postData);
    req.end();
};