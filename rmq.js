'use strict';

var index       = require('./utils'),
    getLogger   = index.getLogger,
    async       = require('async'),
    amqp        = require('amqplib/callback_api'),
    mf          = require('./my-functions');

var RabbitMQ = {
        checkQueue : {
            interval : 300000,
            repeat : true
        }
    };
    
const DEFAULT_RABBITMQ_DATA = {
    requiredQueues : [],    
    queue : {
        connectionOptions : {
            protocol : 'amqp',
            user : 'guest',
            password : 'guest',
            host : '127.0.0.1',
            port : 5672
        },
        assertOptions : {}
    }
};

// RabbitMQ functions

var createRMQChannel = function(options, callback) {
    async.waterfall([
        function prepareConnectionString(prepareCallback) {
            var url = '';

            getLogger().info('Preparing connection URL');

            url += ('protocol' in options) ? options.protocol : DEFAULT_RABBITMQ_DATA.queue.connectionOptions.protocol;
            url += '://';
            url += ('user' in options) ? options.user : DEFAULT_RABBITMQ_DATA.queue.connectionOptions.user;
            url += ':';
            url += ('password' in options) ? options.password : DEFAULT_RABBITMQ_DATA.queue.connectionOptions.password;
            url += '@';
            url += ('host' in options) ? options.host : DEFAULT_RABBITMQ_DATA.queue.connectionOptions.host;
            url += ':';
            url += ('port' in options) ? options.port : DEFAULT_RABBITMQ_DATA.queue.connectionOptions.port;

            getLogger().debug(url);

            prepareCallback(null, url);
        },
        function connect(url, connectCallback) {
            getLogger().info('Connecting to RabbitMQ host');
            amqp.connect(url, connectCallback);
        },
        function createChannel(connection, createCallback) {
            getLogger().info('Creating connection channel');
            connection.createConfirmChannel(function(err, channel) {
                createCallback(err, connection, channel);
            });
        }
    ], callback);
};
exports.createRMQChannel = createRMQChannel;

var assertRMQ = function(ch, qname, options, callback) {
    getLogger().info('Asserting RabbitMQ queue ' + qname);
    // When RabbitMQ quits or crashes it will forget the queues and messages unless you tell it not to. 
    // Two things are required to make sure that messages aren't lost: 
    // we need to mark both the queue and messages as durable.
    ch.assertQueue(qname, options);
    callback(null);
};
exports.assertRMQ = assertRMQ;

var sendRMQMessage = function(qname, msg, options, callback) {
    async.waterfall([
        function getChannel(channelCallback) {
            getMQChannel(channelCallback);
        },
        function sendMessage(channel, messageCallback) {
            options.correlationId = mf.strRandom(32);
            getLogger().info('Sending RabbitMQ message');
            getLogger().trace(JSON.stringify(msg));
            // At this point we're sure that the task_queue queue won't be lost even if RabbitMQ restarts. 
            // Now we need to mark our messages as persistent - by using the persistent option Channel.sendToQueue takes.
            // Note: on Node 6 Buffer.from(msg) should be used
            channel.sendToQueue(qname, new Buffer(msg), options, function(err, ok) {
                if (err) {
                    getLogger()._error(err);
                } else {
                    getLogger().info("RabbitMQ message sent. ID:", options.correlationId);
                };
                messageCallback(err, options.correlationId);
            });
        }
    ], function(err, result) {
        callback(err, result);    
    });
};
exports.sendRMQMessage = sendRMQMessage;

var consumeRMQMessage = function(ch, qname, options, listener, callback) {
    ch.consume(qname, function(msg) {
        getLogger().info("Received RabbitMQ message:");
        if ('correlationId' in msg.properties) {
            getLogger().info('Message Id: ' + msg.properties.correlationId);
        };
        getLogger().debug(JSON.stringify(msg.content));
        getLogger().trace(msg.content.toString());
        getLogger().debug(JSON.stringify(msg.properties));
        listener(msg);
    }, options, callback);
};
exports.consumeRMQMessage = consumeRMQMessage;

var disconnectRMQ = function(conn, callback) {
    getLogger().info('Disconnecting from RabbitMQ');
    conn.close(callback);
};
exports.disconnectRMQ = disconnectRMQ;

var sendRMQack = function(msg, callback) {
    getLogger().info('sendRMQack');
    // If a #consume or #get is issued with noAck: false (the default), 
    // the server will expect acknowledgements for messages before forgetting about them. 
    // If no such acknowledgement is given, those messages may be requeued once the channel is closed.

    async.waterfall([
        function getChannel(channelCallback) {
            getMQChannel(channelCallback);
        },
        function ackMessage(channel, messageCallback) {
            getLogger().info('Send acknowledgement');
            channel.ack(msg);
            messageCallback(null);
        }
    ], function(err) {
        if (!err) {
            getLogger().info('Complete');
        };
        if (callback) {
            callback(err);    
        };
    });
};
exports.sendRMQack = sendRMQack;

var sendRMQnack = function(msg, callback) {
    getLogger().info('sendRMQnack');
    // If a #consume or #get is issued with noAck: false (the default), 
    // the server will expect acknowledgements for messages before forgetting about them. 
    // If no such acknowledgement is given, those messages may be requeued once the channel is closed.

    async.waterfall([
        function getChannel(channelCallback) {
            getMQChannel(channelCallback);
        },
        function ackMessage(channel, messageCallback) {
            getLogger().info('Send nacknowledgement');
            channel.nack(msg);
            messageCallback(null);
        }
    ], function(err) {
        if (!err) {
            getLogger().info('Complete');
        };
        if (callback) {
            callback(err);    
        };
    });
};
exports.sendRMQnack = sendRMQnack;

// создание необходимых очередей
var createRequiredQueues = function(queues, callback) {
    async.each(queues, function(queue, eachCallback) {
        getLogger().info('createRequiredQueue - ' + queue);
        assertRMQ(RabbitMQ.ch, queue, RabbitMQ.data.queue.assertOptions, eachCallback);
    }, callback);
};

// добавление необходимых обработчиков сообщений
var addRequiredListeners = function(listeners, callback) {
    async.each(listeners, function(listener, eachCallback) {
        if (('name' in listener) && ('options' in listener) && ('worker' in listener)) {
            getLogger().info('Add RMQ Listener : ' + listener.name);
            consumeRMQMessage(RabbitMQ.ch, listener.name, listener.options, listener.worker, eachCallback);
        } else {
            eachCallback('No required fields in listener');
        };
    }, callback);
};

// optional error listeners
var channelErrorListener = function(err, callback) {
    getLogger().error('RabbitMQ Cnannel ERROR', err );
    
    if ('code' in err) {
        if (err.code == 'EHOSTUNREACH') {
            RabbitMQ.ch = null;
        };
    };

    if (callback) {
        getLogger().info('Call custom error listener');
        callback(err);
    };
};

// set default RabbitMQ data
var setRMQDefaults = function() {
    getLogger().info('Set RMQ default data');
    RabbitMQ.data = JSON.parse(JSON.stringify(DEFAULT_RABBITMQ_DATA));
};

// сохранение необходимых настроек для RabbitMQ, для использования функции prepareQueue
// предполагаемый пакет настроек
// {
//     requiredQueues : [name1, name2, ...],    
//     queue : {
//         connectionOptions : {},
//         assertOptions : {}
//     },                                          - не обязательный параметр
//     customChannelErrorListener : function() {}, - не обязательный параметр
//     customChannelReadyListener : function() {}, - не обязательный параметр
//     requiredListeners : [ 
//         {
//             name : "queue_name",
//             options : {},                       - queue consume options
//             worker : function() {}              - обработчик сообщения
//         }
//     ]                                           - не обязательный параметр
// }
var setRMQData = function(data, callback) {
    getLogger().info('setRMQData');
    getLogger().trace(JSON.stringify(data));

    setRMQDefaults();

    if ('requiredQueues' in data) {
        if (Array.isArray(data.requiredQueues)) {
            RabbitMQ.data.requiredQueues = data.requiredQueues;
        };
    };
    if ('queue' in data) {
        if ('connectionOptions' in data.queue) {
            RabbitMQ.data.queue.connectionOptions = data.queue.connectionOptions;
        };
        if ('assertOptions' in data.queue) {
            RabbitMQ.data.queue.assertOptions = data.queue.assertOptions;
        };
    };

    if ('requiredListeners' in data) {
        if (Array.isArray(data.requiredListeners)) {
            RabbitMQ.data.requiredListeners = data.requiredListeners;
        };
    };

    if ('customChannelErrorListener' in data) {
        if (typeof(data.customChannelErrorListener) == 'function') {
            RabbitMQ.data.customChannelErrorListener = data.customChannelErrorListener;
        };
    };

    if ('customChannelReadyListener' in data) {
        if (typeof(data.customChannelReadyListener) == 'function') {
            RabbitMQ.data.customChannelReadyListener = data.customChannelReadyListener;
        };
    };

    callback(null);
};
exports.setRMQData = setRMQData;

// подготовка канала до RabbitMQ и создание необходимых подписчиков
var prepareQueue = function(callback) {
    if (!'data' in RabbitMQ) {
        setRMQDefaults();
    };

    var data = RabbitMQ.data;
    getLogger().info('prepareQueue');

    async.waterfall([
        function connect(connectCallback) {
            getLogger().trace(JSON.stringify(data.queue.connectionOptions));
            createRMQChannel(data.queue.connectionOptions, function(err, connection, channel) {
                var error = err;
                if (connection && channel) {
                    RabbitMQ.conn = connection;
                    RabbitMQ.ch = channel;
                } else {
                    if (!err) {
                        error = "Connection && Channel undefined";
                    }
                };
                connectCallback(error);
            });
        },
        function createQueues(createCallback) {
            getLogger().info('createQueues');
            createRequiredQueues(data.requiredQueues, createCallback);
        },
        function configureReportListener(configureCallback) {
            // The prefetch method with the value of 1 tells RabbitMQ not to give more than one message to a worker at a time. 
            // Or, in other words, don't dispatch a new message to a worker until it has processed and acknowledged 
            // the previous one. Instead, it will dispatch it to the next worker that is not still busy.
            RabbitMQ.ch.prefetch(1);

            // Add channel error listener
            var customListener = null;
            if ('customChannelErrorListener' in data) {
                customListener = data.customChannelErrorListener;
            }
            RabbitMQ.ch.on('error', function(err) {
                channelErrorListener(err, customListener);
            });

            configureCallback(null);
        },
        function addReportListener(addCallback) {
            getLogger().info('Check required listeners');
            if ('requiredListeners' in data) {
                addRequiredListeners(data.requiredListeners, addCallback);    
            } else {
                getLogger().info('No listeners required');
                addCallback(null);
            };
        }
    ], function(err) {
        if (err) {
            var custom = null;
            getLogger()._error("Error prepare RabbitMQ");

            if ('customChannelErrorListener' in data) {
                custom = data.customChannelErrorListener;
            };

            channelErrorListener(err, custom);
        } else {
            getLogger().info('Preparing RabbitMQ Complete !');
            if ('customChannelReadyListener' in data) {
                data.customChannelReadyListener();
            };
        };
        callback(err);
    });
};
exports.prepareQueue = prepareQueue;

// получение соединения до очереди RabbitMQ
var getMQConnection = function(callback) {
    var error = 'RabbitMQ connection not ready',
        reconnect = true;
    getLogger().info('getMQConnection');
    if ('conn' in RabbitMQ) {
        if (RabbitMQ.conn) {
            reconnect = false;
        };
    };

    if (!reconnect) {
        getLogger().info('RabbitMQ connection OK');
        callback(null, RabbitMQ.conn);
    } else { 
        getLogger().info('RabbitMQ connection lost');
        callback(error, null);
    };
};
exports.getMQConnection = getMQConnection;

// проверка канала до очереди RabbitMQ, запуск нового соединения при необходимости
var getMQChannel = function(callback) {
    var error = 'RabbitMQ channel not ready',
        reconnect = true;
    getLogger().info('getMQChannel');
    if ('ch' in RabbitMQ) {
        if (RabbitMQ.ch) {
            reconnect = false;
        };
    };

    if (!reconnect) {
        getLogger().info('RabbitMQ channel OK');
        callback(null, RabbitMQ.ch);
    } else { 
        getLogger().info('RabbitMQ channel need to reconnect');
        prepareQueue(function(err) {
            if (err) {
                getLogger().info(error);
                callback(error, null);
            } else {
                callback(null, RabbitMQ.ch);
            };
        });
    };
};
exports.getMQChannel = getMQChannel;

// проверка канала до очереди RAbbitMQ с интервалом
var checkQueue = function() {
    getLogger().info('checkQueue planned - interval ' + RabbitMQ.checkQueue.interval);
    setTimeout(function() {
        getLogger().info('checkQueue');
        getMQChannel(function(err) {
            if (RabbitMQ.checkQueue.repeat) {
                checkQueue();
            };
        });
    }, RabbitMQ.checkQueue.interval);
};

// изменение интервала проверки канала и запуск проверки с интервалом
var createCheckQueueInterval = function(interval, stop) {
    if (interval) {
        getLogger().info('Create checkQueue planned - interval ' + interval);
        RabbitMQ.checkQueue.interval = interval;
    } else {
        getLogger().info('Create checkQueue default planned - interval ' + RabbitMQ.checkQueue.interval);
    };

    if (!stop) {
        RabbitMQ.checkQueue.repeat = true;
        checkQueue(); 
    };
};
exports.createCheckQueueInterval = createCheckQueueInterval;

var stopCheckQueueInterval = function() {
    getLogger().info('Stop checkQueue planned task');
    RabbitMQ.checkQueue.repeat = false;
    getLogger().trace('Check interval task flag - ' + RabbitMQ.checkQueue.repeat);
};
exports.stopCheckQueueInterval = stopCheckQueueInterval;