'use strict';

var nodemailer = require('nodemailer');

var setNodemailTransport = function(transportOptions) {
    // transportOptions = {
    //     host : 'smtp.cbs.kz',
    //     port : 25,
    //     tls: {
    //         rejectUnauthorized: false
    //     }
    // };

    logger.debug('SET NODEMAILER TRANSPORT');
    logger.debug(transportOptions);
    transporter = nodemailer.createTransport(transportOptions);
};
exports.setNodemailTransport = setNodemailTransport;

var sendEmail = function(mailOptions, callback) {
    // var mailOptions = {
    //     from: message.from, // sender address
    //     to: message.to, // list of receivers
    //     subject: message.sbj, // Subject line
    //     // text: message.text // plaintext body
    //     html: message.html
    // };

    logger.info("sending Email");

    // send mail with defined transport object
    transporter.sendMail(mailOptions, function(error, info){
        if (error) {
            logger._error(error);
        };
        callback(error, info);
    });
};
exports.sendMail = sendEmail;